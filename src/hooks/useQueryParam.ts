import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import { parseQuery } from '../core/utils';

const useQueryParam = () : any => {
  const [ query, setQuery ] = useState({});
  const { search } = useLocation();

  useEffect(() => {
    setQuery(parseQuery(search));
  }, [search]);

  return query;
};

export default useQueryParam;