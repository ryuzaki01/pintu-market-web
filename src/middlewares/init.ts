import { QueryClient } from 'react-query';
import { i18n, sanitizeLang } from "../core/lang";
import {NextFunction, Request, Response} from "express";
import defaultQuery from "../core/defaultQuery";

export default function initMiddleware(req : Request, res: Response, next: NextFunction) : void {
  const lang = sanitizeLang(req.headers.lang || req.cookies.lang);

  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        queryFn: defaultQuery,
      },
    },
  })

  res.locals = {
    queryClient,
    data: {
      app: {
        lang,
        query: req.query,
        pathname: req.path,
        params: req.params,
        userAgent: req.headers['user-agent'],
        isMobile: req.useragent.isMobile,
        originalUrl: req.headers.referrer,
      },
      account: {
        loading: false,
        loaded: false,
        data: {}
      }
    }
  };

  global.i18n = i18n(lang);

  return next()
}
