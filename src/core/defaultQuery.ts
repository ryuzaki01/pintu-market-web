import fetch from "isomorphic-fetch";

export default (request) => {
  const { queryKey } = request;

  return fetch(`https://api.pintu.co.id/v2${queryKey[0]}`).then(res => res.json()).catch(e => ({
    code: 'error',
    message: e.message,
    payload: []
  }));
};