import serialize from 'serialize-javascript';
import config from 'config';

interface StateType {
  initState: Record<string, unknown>;
  initCacheState: Record<string, unknown>;
}

export default function end({ initState = {}, initCacheState = {} }: StateType, scriptTags: string): string {
  return `</div>
    <div id="modal-root"><!--MODAL--></div>
    <div id="drawer-root"><!--DRAWER--></div>
    
    <script type="text/javascript">
      window.__data = ${serialize(initState)};
      window.__cache = ${serialize(initCacheState)};
    </script>
    
    <script type="text/javascript">
      window.CONFIG = ${serialize((config as IConfig).globals)}
    </script>    
    
    ${scriptTags}
  </body>
</html>`;
}
