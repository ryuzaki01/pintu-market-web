let debounceTimeout;

interface DebounceOptions {
  leading?: boolean;
  trailing?: boolean;
}

export const debounce = (func: () => void, timeout: number, options: DebounceOptions): void => {
  const { leading = false, trailing = true } = options || {};

  if (debounceTimeout) {
    if (!leading && !trailing) {
      func();
    }

    if (!trailing) {
      return;
    }

    clearTimeout(debounceTimeout);
    debounceTimeout = undefined;
  }

  if (leading && !debounceTimeout) {
    func();
  }

  debounceTimeout = setTimeout(trailing ? func : () => {
    clearTimeout(debounceTimeout);
    debounceTimeout = undefined;
  }, timeout);
};

export const currency = (symbol: string, number: string, lang?: string) : string => {
  const parsedNumber = parseInt(number, 10);
  const negativeNumber = parsedNumber < 0;

  return `${negativeNumber ? '- ' : ''}${symbol ? `${symbol} ` : ''}${String(
    Math.abs(parsedNumber) || 0
  ).replace(/(\d)(?=(\d{3})+(?!\d))/g, symbol === 'USD' || lang === 'en' ? '$1,' : '$1.')}`;
};

export const parseQuery = (subject: string) : Record<string, unknown> => {
  const results = {};
  const parser = /[^&?]+/g;
  let match = parser.exec(subject);

  while (match !== null) {
    const parts = match[0].split('=');

    results[parts[0]] = parts[1];
    match = parser.exec(subject);
  }

  return results;
};
