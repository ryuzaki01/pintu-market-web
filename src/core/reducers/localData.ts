import { parseQuery } from '../utils';

export interface ActionType {
  type: 'query'
}

export default (state: Record<string, unknown>, action: ActionType) : Record<string, unknown> => {
  switch (action.type) {
    case 'query':
      return {
        ...state,
        // eslint-disable-next-line no-restricted-globals
        query: parseQuery((location || {}).search),
        // eslint-disable-next-line no-restricted-globals
        pathname: (location || {}).pathname
      }
    default:
      return state;
  }
};