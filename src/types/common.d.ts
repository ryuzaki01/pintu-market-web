declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.svg';

declare module '*.scss' {
  const content: {[className: string]: string};
  export = content;
}

declare interface IConfig {
  env?: string;
  host?: string;
  port?: string | number;
  secret?: string;
  version?: string;
  Hostname?: string;
  domain?: string;
  cookie?: any;

  // ----------------------------------
  // Project Structure
  // ----------------------------------
  path_base?: string;
  dir_client?: string;
  dir_build?: string;
  dir_public?: string;
  dir_server?: string;

  utils_paths?: any;

  logDir?: string;
  logFileName?: string;
  compiler_public_path?: string;

  globals?: any;
}

// Interfaces
declare interface ILocalData {
  store: any,
  dispatch: any
}

declare interface NodeModule {
  hot: any;
}

declare interface String {
  format(...args: string[]) : string;
}

// Namespaces
declare namespace CONFIG {
  const Hostname: string;
  const defaultTitle: string;
  const titleTemplate: string;
}

declare interface Window {
  __cache: any;
  __data: any;
  i18n: (key: string, ...args: string[]) => string;
  isMobile: boolean;
  isDesktop: boolean;
}