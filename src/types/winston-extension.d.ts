import { Winston as BaseWinston, Transports } from "winston";

interface TransportsCustom {
    File: any;
    Console: any;
    Rotate: any;
}

declare module 'winston' {
    namespace Winston {
        type transports = Transports & TransportsCustom;
    }

    interface Winston extends BaseWinston {
        transports: Transports & TransportsCustom;
    }
}

export {};