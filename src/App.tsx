import React, { useMemo, useReducer } from "react";
import { hot } from 'react-hot-loader/root';
import { renderRoutes } from "react-router-config";
import { BrowserRouter } from "react-router-dom";

import { LocalDataContext } from './core/context';
import localDataReducer from './core/reducers/localData';

import routes from "./routes";

const App = () => {
  const [store, dispatch] = useReducer(localDataReducer, window.__data || {});
  const contextValue = useMemo(() => ({ store, dispatch }), [store, dispatch]);

  return (
    <LocalDataContext.Provider value={contextValue}>
      <BrowserRouter>
        {renderRoutes(routes)}
      </BrowserRouter>
    </LocalDataContext.Provider>
  );
};

export default process.env.DEV_SERVER ? hot(App) : App;