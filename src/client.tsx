/**
 * THIS IS THE ENTRY POINT FOR THE CLIENT, JUST LIKE server.ts IS THE ENTRY POINT FOR THE SERVER.
 */

import "whatwg-fetch";
import "raf/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { loadableReady } from '@loadable/component';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query'

import App from "./App";
import ErrorBoundary from './components/ErrorBoundary';
import { i18n } from "./core/lang";
import cookie from "./helpers/cookie";
import defaultQuery from "./core/defaultQuery";

const MOUNT_NODE = document.getElementById('app');

const {
  lang = cookie.get('userlang'),
  isMobile,
} = window.__data;

const dehydratedState = window.__cache

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: defaultQuery,
    },
  },
})

const render = (TheApp) => {
  window.i18n = i18n(lang);
  window.isMobile = isMobile;
  window.isDesktop = !isMobile;

  loadableReady(() => {
    ReactDOM.hydrate(
      <QueryClientProvider client={queryClient}>
        <Hydrate state={dehydratedState}>
          <ErrorBoundary>
            <TheApp />
          </ErrorBoundary>
        </Hydrate>
      </QueryClientProvider>,
      MOUNT_NODE);
  }).catch(console.error);
};

render(App);
