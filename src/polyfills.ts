// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
Promise.prototype.finally = Promise.prototype.finally || {
    finally (fn) {
      const onFinally = value => Promise.resolve(fn()).then(() => value);

      return this.then(
        result => onFinally(result),
        reason => onFinally(Promise.reject(reason))
      );
    }
  }.finally;

if (typeof Object.entries !== 'function') {
  Object.entries = (obj) => {
    const props = Object.keys(obj);
    let i = props.length;
    const resArray = new Array(i);

    while (i--) {
      resArray[i] = [props[i], obj[props[i]]];
    }

    return resArray;
  };
}

Array.prototype.includes = Array.prototype.includes || function(search){
  return !!~this.indexOf(search);
};
