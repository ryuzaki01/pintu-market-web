import React from 'react';
import { render, screen } from '@testing-library/react';

import NotFound from '../';

test('Should render NotFound correctly', () => {
	render(<NotFound />)

  expect(screen.getByRole('heading')).toHaveTextContent('404 NOT FOUND')
});
