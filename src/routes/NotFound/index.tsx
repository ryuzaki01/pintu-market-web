import React, {ReactElement} from 'react';

const NotFound = () : ReactElement => {
  return (
    <div>
      <div>
        <div>
          <h1>404 NOT FOUND</h1>
        </div>
      </div>
    </div>
  );
};

export default NotFound;