import React, {ReactElement} from 'react';
import Svg from "../../../components/Svg";
import AnimatedText from "../../../components/AnimatedText";
import {object, oneOf} from "prop-types";
import {Currency, Price, PriceInterval} from "../";

export interface CurrencyItemProps {
  data?: Currency;
  price?: Price;
  interval?: PriceInterval
}

const getValueByInterval = (interval: PriceInterval, price: Price) => {
  const { day, week, month, year } = price || {};

  if (interval === 'week') {
    return week;
  }

  if (interval === 'month') {
    return month;
  }

  if (interval === 'year') {
    return year;
  }

  return day;
}

const CurrencyItem = (props: CurrencyItemProps) : ReactElement => {
  const { data, price, interval } = props;
  const {
    currencySymbol,
    name,
    logo,
    color
  } = data || {};
  const { latestPrice, day, week, month, year } = price || {};

  return (
    <tr data-testid="currency-item">
      <td>
        <Svg url={logo} color={color}/>
      </td>
      <td>
        {name}
        <div className="d-lg-none">
          {currencySymbol}
        </div>
      </td>
      <td className="d-none d-lg-table-cell">
        {currencySymbol}
      </td>
      <td className="text-end">
        <AnimatedText fade type="currency">
          {latestPrice}
        </AnimatedText>
        <div className="d-lg-none text-end">
          <AnimatedText type="percentage">
            {getValueByInterval(interval, price)}
          </AnimatedText>
        </div>
      </td>
      <td className="d-none d-lg-table-cell">
        <AnimatedText type="percentage">
          {day}
        </AnimatedText>
      </td>
      <td className="d-none d-lg-table-cell">
        <AnimatedText type="percentage">
          {week}
        </AnimatedText>
      </td>
      <td className="d-none d-lg-table-cell">
        <AnimatedText type="percentage">
          {month}
        </AnimatedText>
      </td>
      <td className="d-none d-lg-table-cell">
        <AnimatedText type="percentage">
          {year}
        </AnimatedText>
      </td>
    </tr>
  )
}

CurrencyItem.propTypes = {
  data: object,
  price: object,
  interval: oneOf(['day', 'week', 'month', 'year'])
}

export default CurrencyItem;