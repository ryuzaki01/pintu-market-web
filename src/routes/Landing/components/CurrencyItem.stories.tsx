import React, {useEffect, useState} from 'react';
import {Meta, Story} from '@storybook/react';
import {Table} from "react-bootstrap";

import CurrencyItem, { CurrencyItemProps } from './CurrencyItem';
import {PriceInterval} from "../";

export default {
  title: 'Components/CurrencyItem',
  component: CurrencyItem,
  argTypes: {
    interval: {
      options: ['day', 'week', 'month', 'year'],
      control: {
        type: 'select'
      }
    },
  }
} as Meta;

const Template: Story<CurrencyItemProps> = (args) => {
  const [interval, setInterval] = useState<PriceInterval>(args.interval);

  useEffect(() => {
    setInterval(args.interval);
  }, [args.interval])

  return (
    <Table responsive>
      <thead>
      <tr>
        <th/>
        <th>CRYPTO</th>
        <th className="d-none d-lg-table-cell"/>
        <th className="text-end">
          {`HARGA `}
          <select className="d-lg-none" value={interval} onChange={e => setInterval(e.target.value as PriceInterval)}>
            <option value="day">24 JAM</option>
            <option value="week">1 MGG</option>
            <option value="month">1 BLN</option>
            <option value="year">1 THN</option>
          </select>
        </th>
        <th className="d-none d-lg-table-cell">24 JAM</th>
        <th className="d-none d-lg-table-cell">1 MGG</th>
        <th className="d-none d-lg-table-cell">1 BLN</th>
        <th className="d-none d-lg-table-cell">1 THN</th>
      </tr>
      </thead>
      <tbody>
        <CurrencyItem {...args} interval={interval} />
      </tbody>
    </Table>
  )
};

export const Default = Template.bind({});
Default.args = {
  interval: 'day',
  data:{ "currencyGroup":"BTC","color":"#F78B1A","currencySymbol":"BTC","name":"Bitcoin","logo":"https://s3-ap-southeast-1.amazonaws.com/static.pintu.co.id/assets/images/logo/circle_BTC.svg","decimal_point":8,"listingDate":"2020-03-11T15:13:52.000Z","wallets":[{"currencyGroup":"BTC","tokenSymbol":"BTC","decimal_point":8,"tokenType":"Bitcoin","blockchain":"Bitcoin","explorer":"https://explorer.bitcoin.com/btc/tx/"}]},
  price:{ "pair":"btc/idr","latestPrice":"299100496","day":"-4.54","week":"0.10","month":"-32.42","year":"-38.48" }
};