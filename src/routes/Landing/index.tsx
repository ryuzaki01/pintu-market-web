import React, {ReactElement, useState} from 'react';
import { useQueries } from 'react-query';
import { Container, Table } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import { ReactQueryDevtools } from 'react-query/devtools'

import LandingHeader from '../../components/Header';
import CurrencyItem from './components/CurrencyItem';

import './style.scss';

export interface Wallet {
  currencyGroup: string;
  tokenSymbol: string;
  decimal_point: number;
  tokenType: string;
  blockchain: string;
  explorer: string;
}

export interface Price {
  pair: string;
  latestPrice: string;
  day: string;
  week: string;
  month: string;
  year: string;
}

export interface Currency {
  currencyGroup: string;
  color: string;
  currencySymbol: string;
  name: string;
  logo: string;
  decimal_point: number;
  listingDate: string;
  wallets: Wallet[];
}

export interface SupportedCurrencyResponse {
  code: string;
  message: string;
  payload: Currency[]
}

export interface PriceChangeResponse {
  code: string;
  message: string;
  payload: Price[]
}

export type PriceInterval = 'day' | 'week' | 'month' | 'year';

const Landing = () : ReactElement => {
  const [currencyResponse, priceResponse] = useQueries([
    {
      queryKey: '/wallet/supportedCurrencies'
    },
    {
      queryKey: '/trade/price-changes',
      refetchInterval: 1000,
    }
  ]);
  const [interval, setInterval] = useState<PriceInterval>('day');

  return (
    <Container fluid className="landing">
      <Helmet>
        <title>Harga Crypto Hari</title>
      </Helmet>
      <LandingHeader />
      <h1>Harga Crypto dalam Rupiah Hari Ini</h1>
      <Table responsive>
        <thead>
          <tr>
            <th/>
            <th>CRYPTO</th>
            <th className="d-none d-lg-table-cell"/>
            <th className="text-end">
              {`HARGA `}
              <select className="d-lg-none" data-testid="select-interval" value={interval} onChange={e => setInterval(e.target.value as PriceInterval)}>
                <option value="day">24 JAM</option>
                <option value="week">1 MGG</option>
                <option value="month">1 BLN</option>
                <option value="year">1 THN</option>
              </select>
            </th>
            <th className="d-none d-lg-table-cell">24 JAM</th>
            <th className="d-none d-lg-table-cell">1 MGG</th>
            <th className="d-none d-lg-table-cell">1 BLN</th>
            <th className="d-none d-lg-table-cell">1 THN</th>
          </tr>
        </thead>
        <tbody>
          {((currencyResponse?.data as SupportedCurrencyResponse)?.payload || []).map((currency, i) => {
            if (i === 0) {
              return;
            }

            const price = ((priceResponse?.data as PriceChangeResponse)?.payload || [])[i - 1];

            return (
              <CurrencyItem
                key={`currency-${currency.currencySymbol}`}
                data={currency}
                price={price}
                interval={interval}
              />
            )
          })}
        </tbody>
      </Table>
      <ReactQueryDevtools initialIsOpen />
    </Container>
  );
};

export default Landing;