import React from 'react';
import { render, screen, cleanup, fireEvent } from '../../../../test-utils';

import Landing from '../';

afterEach(cleanup);

test('Should render Landing correctly', () => {
	render(<Landing />)

  expect(screen.getByRole('heading')).toHaveTextContent('Harga Crypto dalam Rupiah Hari Ini')
});

test('Should render all currency items', async () => {
  render(<Landing />)
  const items = await screen.findAllByTestId('currency-item');
  expect(items).toHaveLength(66)
})

test('Should handle change interval', async () => {
  render(<Landing />)
  const select = await screen.findByTestId('select-interval');

  fireEvent.change(select, {target: {value: 'week'}});
  expect(select).toHaveValue('week');

  fireEvent.change(select, {target: {value: 'month'}});
  expect(select).toHaveValue('month');

  fireEvent.change(select, {target: {value: 'year'}});
  expect(select).toHaveValue('year');
})