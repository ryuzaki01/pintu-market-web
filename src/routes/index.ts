import { RouteConfig } from 'react-router-config'
import Loadable from '@loadable/component';

import Layout from '../components/Layout';

const Landing = Loadable(/* #__LOADABLE__ */ () => import(/* webpackChunkName: "Landing" */ './Landing'));
const NotFound = Loadable(/* #__LOADABLE__ */ () => import(/* webpackChunkName: "NotFound" */ './NotFound'));

export default [
  {
    component: Layout,
    routes: [
      {
        path: "/",
        exact: true,
        component: Landing
      },
      {
        path: "*",
        component: NotFound
      }
    ]
  }
] as RouteConfig[];