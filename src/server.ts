import express from "express";
import cookieParser from "cookie-parser";
import userAgent from "express-useragent";
import winston from "winston";
import config from "config";
import "winston-logrotate";

import initMiddleware from "./middlewares/init";
import AppServer from "./AppServer";

const __PROD__ = process.env.NODE_ENV === 'production';

global.CONFIG = (config as IConfig).globals;

// Configure Default Logger
winston.configure({
  transports: [
    new winston.transports.Console({
      handleExceptions: true,
      humanReadableUnhandledException: true,
      json: process.env.PRETTY_LOG === 'true',
    }),
  ],
  exitOnError: false,
});

if (__PROD__) {
  winston.remove(winston.transports.Console);
  winston.add((winston.transports as any).Rotate, {
    file: `${(config as IConfig).logDir}/${(config as IConfig).logFileName}`,
    colorize: false,
    timestamp: true,
    size: process.env.LOG_SIZE || '100m',
    keep: process.env.LOG_KEEP || 5,
    compress: false,
    json: true,
  });
} else {
  winston.add(winston.transports.File, {
    filename: (config as IConfig).logFileName,
    handleExceptions: true,
    json: true,
  });
}

const server = express();

process.on('uncaughtException', winston.error);
process.on('unhandledRejection', winston.error);

server.use(express.static((config as IConfig).utils_paths.public(), { maxAge: '1y' }));

if (__PROD__) {
  server.set('trust proxy', 1);
  server.disable('x-powered-by');
}

server.use(cookieParser((config as IConfig).secret, (config as IConfig).cookie));
server.use(express.urlencoded({ extended: true }));
server.use(userAgent.express());

server.use('/health', (req, res) => {
  return res.send('OK');
});

server.use(
  initMiddleware,
  AppServer
);

server.use((_, res) => res.status(404).send('Not Found'));

process.on('SIGINT', () => {
  winston.info('Received SIGINT exiting');
  process.exit();
});

//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module && module.hot) {
  (server as any).hot = module.hot;
  module.hot.accept();
} else {
  server.listen((config as IConfig).port, () => {
    winston.info(`Server is up and listening at ${(config as IConfig).host}:${(config as IConfig).port} env:${process.env.NODE_ENV}`);
  });
}

export default server;
