import React from 'react';
import {Meta, Story} from '@storybook/react';

import Svg, { SvgProps } from './';

export default {
  title: 'Components/Svg',
  component: Svg,
  argTypes: {
    color: {
      control: {
        type: 'color'
      }
    }
  },
} as Meta;

const Template: Story<SvgProps> = (args) => <Svg {...args} />;

export const Default = Template.bind({});
Default.args = {
  color: 'yellow',
  url: 'https://s3-ap-southeast-1.amazonaws.com/static.pintu.co.id/assets/images/logo/circle_IDRT.svg'
};