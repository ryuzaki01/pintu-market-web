import React, {ReactElement, useEffect, useState} from 'react';

export interface SvgProps {
  url: string;
  color?: string;
}

const Svg = (props: SvgProps) : ReactElement => {
  const { url, color } = props;
  const [svg, setSvg] = useState(undefined);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!url) {
      return;
    }

    fetch(url)
      .then(res => res.text())
      .then(setSvg)
      .catch(() => setSvg(undefined))
      .finally(() => {
        setLoading(false);
      });
  }, [url])

  if (loading || !svg) {
    return <div/>;
  }

  return <div style={{ color: color }} dangerouslySetInnerHTML={{ __html: svg }}/>;
}

export default Svg;
