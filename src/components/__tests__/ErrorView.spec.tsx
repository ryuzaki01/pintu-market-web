import React from 'react';
import {render, screen} from '../../../test-utils';

import ErrorView, { CustomError } from '../ErrorBoundary/ErrorView';
import {act} from "react-dom/test-utils";

test('Should render ErrorView correctly', () => {
  const error = new Error('Test Error');

	render(
    <ErrorView error={error} info={{ componentStack: '' }}/>
  )
});

test('Should handle error link correctly', () => {
  const assignMock = jest.fn();

  delete window.location;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.location = { assign: assignMock };
  const error: CustomError = new Error('Test Error');
  error.link = 'https://blabla';
  error.image = 'https://blabla';

  render(
    <ErrorView error={error} info={{ componentStack: '' }}/>
  )

  act(() => {
    screen.getByTestId('error-btn').click();
  })
});