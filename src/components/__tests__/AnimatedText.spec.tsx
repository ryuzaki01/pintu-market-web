import React from 'react';
import { render, waitFor, cleanup, act } from '../../../test-utils';

import AnimatedText from '../AnimatedText';

afterEach(cleanup);

test('Should render AnimatedText correctly', async () => {

  const { getByText } = render(
    <AnimatedText type="percentage">{`12`}</AnimatedText>
  )

  expect(getByText('+12.00%')).toBeDefined();
});

test('Should render currency correctly', async () => {

  const { getByText, rerender } = render(
    <AnimatedText fade type="currency">{`120000000`}</AnimatedText>
  )

  expect(getByText('Rp 120.000.000')).toBeDefined();

  rerender(
    <AnimatedText fade type="currency">{`100000000`}</AnimatedText>
  )

  expect(getByText('Rp 100.000.000')).toBeDefined();

  rerender(
    <AnimatedText fade type="currency">{`150000000`}</AnimatedText>
  )

  expect(getByText('Rp 150.000.000')).toBeDefined();
});
