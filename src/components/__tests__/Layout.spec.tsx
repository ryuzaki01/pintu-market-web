import React, {useReducer} from 'react';
import { render, act } from '../../../test-utils';

import Layout from '../Layout';
import routes from '../../routes'
import {BrowserRouter} from "react-router-dom";
import {LocalDataContext} from "../../core/context";

test('Should render Layout correctly', () => {
  const store = {};
  const dispatch = jest.fn();
	render(
    <LocalDataContext.Provider value={({ store, dispatch })}>
      <BrowserRouter>
        <Layout route={{ routes }} />
      </BrowserRouter>
    </LocalDataContext.Provider>
  )
});