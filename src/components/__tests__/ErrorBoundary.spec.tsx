import React from 'react';
import {render} from '../../../test-utils';

import ErrorBoundary from '../ErrorBoundary';

test('Should render ErrorBoundary correctly', async () => {
  const spy = jest.spyOn(console, 'error')
  spy.mockImplementation(() => {
    //empty
  })
  const Throw = () => {
    throw new Error('Test')
  }

  const { getByText } = render(
    <ErrorBoundary>
      <Throw/>
    </ErrorBoundary>
  )

  expect(getByText('Test')).toBeDefined()
  spy.mockRestore();
})