import React, {Component, ErrorInfo, ReactElement} from 'react';
import { bool, func, node, string } from 'prop-types';
import ErrorView, {CustomError, InfoView} from './ErrorView';

type Props = {
  children: ReactElement,
  debug: boolean,
  errorMessage: string
};

type State = {
  error: CustomError | boolean,
  info: InfoView | boolean
}

class ErrorBoundary extends Component<Props, State> {
  static propTypes = {
    children: node,
    debug: bool,
    errorMessage: string,
    render: func,
  };

  static defaultProps = {
    debug: false,
    errorMessage: '',
    render: null,
  };

  state = {
    error: false,
    info: false,
  };

  /* istanbul ignore next */
  componentDidCatch(error: Error, info: ErrorInfo) : void {
    if (this.props.debug) {
      console.groupCollapsed(`Error occured!`);
      console.log('Error:', error);
      console.log('Info:', info);
      console.groupEnd();
    }

    this.setState({ error, info });
  }

  renderError = (error: Error, info: ErrorInfo) : ReactElement => {
    return <ErrorView error={error} info={info}/>
  };

  render() : ReactElement {
    const { error, info } = this.state;
    const { children } = this.props;

    /* istanbul ignore else */
    return this.state.error ? /* istanbul ignore next */ this.renderError((error as unknown) as Error, (info as unknown) as ErrorInfo) : children;
  }
}

export default ErrorBoundary;
