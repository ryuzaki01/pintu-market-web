import React, {ReactElement} from 'react';
import { func, node, object } from 'prop-types';

const isDev = process.env.NODE_ENV === 'development';

export type CustomError = {
  message: string;
  link?: string;
  image?: string;
  buttonText?: string;
}

export type InfoView = {
  componentStack: string;
}

export interface ErrorViewProps {
  buttonActionHandler?: () => void;
  buttonText?: string;
  error: CustomError
  info: InfoView
}

const ErrorView = (props: ErrorViewProps) : ReactElement => {
  const { buttonActionHandler, buttonText, error, info } = props;
  const finalButtonActionHandler = error.link ?
    () => {
      if(typeof window !== "undefined") {
        window.location.href = error.link;
      }
    } : buttonActionHandler;

  return (
    <div data-testid="errorview" className="error-boundary-wrapper">
      <div className="error-boundary-box">
        {error.image && (
          <img className="error-boundary-image" src={error.image}  alt="error-image"/>
        )}
        <p
          className="error-boundary-text"
          dangerouslySetInnerHTML={{ __html: error.message }}
        />
        <a className="btn" data-testid="error-btn" onClick={finalButtonActionHandler}>
          {error.buttonText || buttonText || 'Retry'}
        </a>
        {/* eslint-disable */ isDev && (
        <div className="error-boundary-stack">
    <pre>{info.componentStack}</pre>
        </div>)/* eslint-enable */}
      </div>
    </div>
  );
}

ErrorView.propTypes = {
  buttonActionHandler: func,
  buttonText: node,
  history: object,
  error: object,
  info: object
};

export default ErrorView;
