import React, {ReactElement} from 'react';
import { object } from 'prop-types';
import {renderRoutes, RouteConfig} from 'react-router-config';

import '../../styles/index.scss';

export interface LayoutProps {
  route: RouteConfig;
}

const Layout = ({ route } : LayoutProps) : ReactElement => {
  return (
    <div style={{ height: '100vh' }}>
      {renderRoutes(route.routes)}
    </div>
  );
};

Layout.propTypes = {
  route: object,
};

export default Layout;
