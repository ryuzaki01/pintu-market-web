import React, {ReactElement, useEffect, useState} from 'react';
import { bool, node, oneOf } from 'prop-types';
import { currency } from "../../core/utils";

import './styles.scss';

export interface AnimatedTextProps {
  fade?: boolean,
  type: 'currency' | 'percentage'
  children?: string
}

const getText = (value, type) => {
  if (type === 'currency') {
    return currency('Rp', value);
  }

  const parsedValue = parseFloat(value) || 0.00;

  return `${parsedValue > 0 ? '+' : ''}${parsedValue.toFixed(2)}%`
}

const AnimatedText = (props: AnimatedTextProps): ReactElement => {
  const { children, type, fade } = props;
  const parsedValue = parseFloat(children);
  const [value, setValue] = useState(parsedValue);
  const [diff, setDiff] = useState(0);

  const getTextColor = (value, type, diff, fade) => {
    const parsedValue = parseFloat(value);
    const colorDiff = type === 'currency' ? diff : parsedValue;

    if (colorDiff > 0.00) {
      return fade ? 'text-fade-green' : 'text-green';
    }

    if (colorDiff < 0.00) {
      return fade ? 'text-fade-red' : 'text-red';
    }

    return ''
  }

  useEffect(() => {
    const newValue = parseFloat(children);
    setDiff(newValue - value);
    setValue(newValue);
  }, [children]);

  return <span className={['animated-text', getTextColor(children, type, diff, fade)].filter(Boolean).join(' ')}>{getText(children, type)}</span>
};

AnimatedText.propTypes = {
  fade: bool,
  type: oneOf(['currency', 'percentage']),
  children: node
}

export default AnimatedText;