import React, {useEffect, useState} from 'react';
import {Meta, Story} from '@storybook/react';

import AnimatedText, { AnimatedTextProps } from './';

export default {
  title: 'Components/AnimatedText',
  component: AnimatedText,
  argTypes: {
    type: {
      options: ['currency', 'percentage'],
      control: {
        type: 'select'
      }
    },
    fade: {
      control: {
        type: 'boolean'
      }
    }
  },
} as Meta;

const Template: Story<AnimatedTextProps> = (args) => <AnimatedText {...args} />;

export const Default = Template.bind({});
Default.args = {
  type: 'percentage',
  children: '100'
};

export const Variants: Story = () => {
  const [value, setValue] = useState(Math.random());
  const [valuePercent, setValuePercent] = useState(Math.random());
  let timer;

  useEffect(() => {
    if (timer) {
      return;
    }

    timer = setInterval(() => {
      const newValue = Math.floor(Math.random() * 10000000) + 10000000
      const newValuePercent = (Math.random() * 201) -100
      setValue(newValue);
      setValuePercent(newValuePercent);
    }, 2000);

    return () => {
      clearInterval(timer);
    }
  }, [])

  return (
    <>
      <div>
        <AnimatedText type="currency">{`${value}`}</AnimatedText>
      </div>
      <div>
        <AnimatedText fade type="currency">{`${value}`}</AnimatedText>
      </div>
      <div>
        <AnimatedText type="percentage">{`${valuePercent}`}</AnimatedText>
      </div>
    </>
  )
}
