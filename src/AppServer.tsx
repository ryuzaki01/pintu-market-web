import React, { useReducer, useMemo } from "react";
import { renderRoutes } from "react-router-config";
import { StaticRouter } from "react-router";
import { renderToNodeStream } from "react-dom/server";
import { ChunkExtractor } from "@loadable/server";
import { dehydrate, Hydrate, QueryClientProvider } from 'react-query';
import ErrorBoundary from './components/ErrorBoundary';

import upperHTML from "./core/html/start";
import lowerHTML from "./core/html/end";
import getStaticAsset from "./core/assets";
import localDataReducer from './core/reducers/localData';
import { LocalDataContext } from './core/context';

import routes from "./routes";
import {Request, Response} from "express";

const statsFile = getStaticAsset('./build/loadable-stats.json', true);

export default async (req: Request, res: Response) : Promise<void> => {
  const { data, queryClient } = res.locals;
  const { isMobile } = data || {};
  const chunkExtractor = new ChunkExtractor({ statsFile, entrypoints: ['client'] });

  await queryClient.prefetchQuery('/wallet/supportedCurrencies')
  const dehydratedState = dehydrate(queryClient)

  global.isMobile = isMobile;
  global.isDesktop = !isMobile;

  const writeLowerHtml = (preloadedState, scriptTags) => {
    res.write(lowerHTML(preloadedState, scriptTags));

    return res.end();
  };

  const App = () => {
    const [store, dispatch] = useReducer(localDataReducer, res.locals.data);
    const contextValue = useMemo(() => ({ store, dispatch }), [store, dispatch]);

    return (
      <QueryClientProvider client={queryClient}>
        <Hydrate state={dehydratedState}>
          <ErrorBoundary>
            <LocalDataContext.Provider value={contextValue as ILocalData}>
              <StaticRouter location={req.url} context={res.locals.data}>
                {renderRoutes(routes)}
              </StaticRouter>
            </LocalDataContext.Provider>
          </ErrorBoundary>
        </Hydrate>
      </QueryClientProvider>
    );
  };

  const jsx = chunkExtractor.collectChunks(<App />);
  const htmlStates = {
    initState: { ...res.locals.data },
    initCacheState: dehydratedState,
  };

  const scriptTags = chunkExtractor.getScriptTags();
  const linkTags = chunkExtractor.getLinkTags();
  const styleTags = chunkExtractor.getStyleTags();

  res.type('html');
  res.write(upperHTML({ linkTags, styleTags }));

  const html = renderToNodeStream(jsx);

  html.pipe(res, { end: false });
  html.on('end', () => {
    writeLowerHtml(htmlStates, scriptTags)
  });
}