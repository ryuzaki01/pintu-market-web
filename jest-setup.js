import 'intersection-observer';
import { i18n } from './src/core/lang';

const isMobile = false;
global.i18n = i18n('id');
global.isMobile = isMobile;
global.isDesktop = !isMobile;