const path = require('path');
const pkg = require('../package.json');

if (!process.env.NODE_ENV) {
  throw new Error('env NODE_ENV not defined');
}

let config = {
  env: process.env.NODE_ENV,
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 3000,
  secret: process.env.SECRET,
  version: pkg.version,
  Hostname: process.env.APPHOST || `http://${process.env.HOST}:${process.env.PORT}`,
  domain: process.env.APPDOMAIN,
  cookie: {
    domain: `.${process.env.APPDOMAIN}`,
    path: '/',
    maxAge: 24 * 60 * 60 * 1000
  },
  GQL_HOST: process.env.GQL_HOST || `http://localhost:${process.env.PORT}/graphql`,

  // ----------------------------------
  // Project Structure
  // ----------------------------------
  path_base: path.resolve(__dirname, '..'),
  dir_client: 'src',
  dir_build: 'build',
  dir_public: 'build/public/',
  dir_server: 'build',
};
const base = (...args) => path.resolve(config.path_base, ...args);
config.utils_paths = {
  base,
  client: base.bind(null, config.dir_client),
  build: base.bind(null, config.dir_build),
  public: base.bind(null, config.dir_public)
};
config.logDir = process.env.LOGDIR || config.path_base;
config.logFileName = `${pkg.name}.log`;
config.compiler_public_path = process.env.PUBLIC_PATH || `/assets/`;
config.globals = {
  // Global Configuration Here
  Hostname: config.Hostname,
};

module.exports = config;