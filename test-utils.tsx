import React, {FC, ReactElement, useMemo, useReducer} from 'react'
import {render, RenderOptions} from '@testing-library/react'
import {dehydrate, Hydrate, QueryClient, QueryClientProvider} from "react-query";
import { BrowserRouter } from "react-router-dom";
import { LocalDataContext } from './src/core/context';
import localDataReducer from './src/core/reducers/localData';
import defaultQuery from "./src/core/defaultQuery";
import ErrorBoundary from "./src/components/ErrorBoundary";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: defaultQuery,
    },
  },
})
const dehydratedState = dehydrate(queryClient)
const AllTheProviders: FC<{children: ReactElement}> = ({children}) => {
  const [store, dispatch] = useReducer(localDataReducer, window.__data || {});
  const contextValue = useMemo(() => ({ store, dispatch }), [store, dispatch]);

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={dehydratedState}>
        <ErrorBoundary>
          <LocalDataContext.Provider value={contextValue}>
            <BrowserRouter>
              {children}
            </BrowserRouter>
          </LocalDataContext.Provider>
        </ErrorBoundary>
      </Hydrate>
    </QueryClientProvider>
  )
}

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, {wrapper: AllTheProviders, ...options})

export * from '@testing-library/react';

export {customRender as render}