FROM node:12.4.0-alpine as pintuweb
ENV APP_NAME=pintuweb
ARG PUBLIC_PATH
ENV PUBLIC_PATH=$PUBLIC_PATH
# Set current timezone into +7
RUN ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

# Install required library
RUN apk add --no-cache --update libpng-dev libjpeg-turbo-dev

# Prepare working directory
RUN mkdir -p /app/${APP_NAME}/build
WORKDIR /app/${APP_NAME}

COPY . .

# Prepare the app
RUN npm install --cache /npm_cache

# Set Build env
ARG NODE_ENV
ENV NODE_ENV=$NODE_ENV

# Build the app
RUN npm run build

# Remove native build dependencies & npm cache
RUN apk del native-deps && rm -rf /npm_cache

# Change ownership to user 1000
RUN chown -R 1000.1000 /app/${APP_NAME}

# Run as user 1000
USER 1000
CMD ["npm", "run", "serve"]